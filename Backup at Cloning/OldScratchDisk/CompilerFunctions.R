
  
  AMP1_means <-function(output_list,sub_nr)
  {
    #loading up results
    results = output_list[[sub_nr]]$AMP_1
    #normalizing: 0 is dislike, 1 is like, -1 is unresponded
    results$answers <- results$answers - 1
    #index for responded and therefore included
    inc <- results$answers >= 0
    
    
    results_inc = results[inc,]
    Tmean = mean(results_inc$answers[results_inc$category== 'Trump'])
    Cmean = mean(results_inc$answers[results_inc$category== 'Clinton'])
    Nmean = mean(results_inc$answers[results_inc$category== 'Neutral'])
    Lmean = mean(results_inc$answers[results_inc$category== 'Lofven'])
    
    if (sum(inc)<30)
    {BadS=TRUE} 
    else if(Tmean +0.7 < Cmean | Cmean + 0.7 < Tmean){BadS = TRUE}
    else if(Tmean >0.9 | Tmean <0.1 | Cmean > 0.9 | Cmean < 0.1){BadS = TRUE}
    else {BadS=FALSE}
    return(data.frame(T1mean = Tmean,C1mean =Cmean,L1mean =Lmean,N1mean =Nmean,BadS = BadS))
  }

  AMP2_means <-function(output_list,sub_nr)
  {
    #loading up results
    results = output_list[[sub_nr]]$AMP_2
    #normalizing: 0 is dislike, 1 is like, -1 is unresponded
    results$answers <- results$answers - 1
    #index for responded and therefore included
    inc <- results$answers >= 0
    
    
    results_inc = results[inc,]
    Tmean = mean(results_inc$answers[results_inc$category== 'Trump'])
    Cmean = mean(results_inc$answers[results_inc$category== 'Clinton'])
    Nmean = mean(results_inc$answers[results_inc$category== 'Neutral'])
    Lmean = mean(results_inc$answers[results_inc$category== 'Lofven'])
    
    #if (sum(inc)<30)
    #{BadS=TRUE} 
    #else if(Tmean +0.7 < Cmean | Cmean + 0.7 < Tmean){BadS = TRUE}
    #else if(Tmean >0.9 | Tmean <0.1 | Cmean > 0.9 | Cmean < 0.1){BadS = TRUE}
    #else {BadS=FALSE}
    return(data.frame(T2mean = Tmean,C2mean =Cmean,L2mean =Lmean,N2mean =Nmean
                      #,BadS = BadS
                      ))
  }  
  who_vote <-function(output_list,sub_nr)
  {
    support_char <- output_list[[sub_nr]]$action[3]
    Support_nr = as.numeric(support_char)
    if (Support_nr ==1)
    {support = "Trump"}
    else if (Support_nr ==2)
    {support = "Clinton"}
    else if (Support_nr ==97)
    {support = "Undecided"}
    else if (Support_nr ==98)
    {support = "No vote"}
    else if (Support_nr ==99)
    {support = "Hidden"}
    else
    {support = "Other"}
    support_df<-data.frame(Support=support)
    return(support_df)
  }
  
demographics <-function(output_list,sub_nr)
{
  dem_vec = output_list[[sub_nr]]$demo
  gender = toupper(dem_vec[1])
  age = as.numeric(dem_vec[2])
  country = dem_vec[3]
  state = toupper(dem_vec[4])
  
  if  (dem_vec [5] == 1)
  {citizen = 'Yes'}
  else if (dem_vec [5] == 2)
  {citizen = 'No'}
  else
  {citizen = 'Hidden'}
  #income
  if (dem_vec[6] == "99")
  {income = "Hidden"}
  else
  {income = dem_vec[6]}
  #race
  race = dem_vec[7]
  if  (dem_vec [7] == 1)
  {race = 'White'}
  else if (dem_vec [7] == 2)
  {race = 'Black'}
  else if (dem_vec [7] == 3)
  {race = 'American Indian'}
  else if (dem_vec [7] == 4)
  {race = 'Asian'}
  else if (dem_vec [7] == 5)
  {race = 'Other'}
  else
  {race = 'Hidden'}
  #hispanic
  if  (dem_vec [8] == 1)
  {hispanic = 'Yes'}
  else if (dem_vec [8] == 2)
  {hispanic = 'No'}
  else
  {hispanic = 'Hidden'}
  #schooling
  schooling = dem_vec[9]
  #married
  if  (dem_vec [10] == 1)
  {married = 'No'}
  else if (dem_vec [10] == 2)
  {married = 'Yes'}
  else if (dem_vec [10] == 3)
  {married = 'Widowed'}
  else if (dem_vec [10] == 4)
  {married = 'Divorced'}
  else if (dem_vec [10] == 5)
  {married = 'Separated'}
  else
  {married = 'Hidden'}
  
  demo_frame = data.frame(Gender = gender, Age = age, Country = country, State = state, Citizen = citizen, Income = income, Race = race, Hispanic = hispanic,Schooling = schooling, Married = married, stringsAsFactors = FALSE)
  return(demo_frame)
}



fiske_compiler<-function(output_list,sub_nr,candidate)
{
  if (candidate== "Trump")
  {results_vec = as.numeric(output_list[[sub_nr]]$TFiske)}
  if (candidate== "Clinton")
  {results_vec = as.numeric(output_list[[sub_nr]]$CFiske)}
  
  confident = results_vec[1]
  warm = results_vec[2]
  independent=results_vec[3]
  intelligent=results_vec[4]
  tolerant = results_vec[5]
  competent = results_vec[6]
  good_natured = results_vec[7]
  competitive = results_vec[8]
  sincere = results_vec[9]
  
  if(candidate == 'Trump')
  {
    fiske_df = data.frame(TFWarm = warm, TFTolerant = tolerant, TFGoodNatured = good_natured, TFSincere = sincere, TFIndependent = independent,TFIntelligent = intelligent, TFCompetent = competent,TFCompetitive = competitive)
  }
  if (candidate == 'Clinton')
  {
    fiske_df = data.frame(CFWarm = warm, CFTolerant = tolerant, CFGoodNatured = good_natured, CFSincere = sincere,CFIndependent = independent,CFIntelligent = intelligent, CFCompetent = competent,CFCompetitive = competitive)
  }
  return(fiske_df)
}

cArraro_compiler<-function(output_list,sub_nr,candidate)
{
  if (candidate== "Trump")
  {results_vec = as.numeric(output_list[[sub_nr]]$TCarra)}
  if (candidate== "Clinton")
  {results_vec = as.numeric(output_list[[sub_nr]]$CCarra)}
  
  active = results_vec[1]
  intelligent = results_vec[2]
  potent=results_vec[3]
  fast=results_vec[4]
  likeable = results_vec[5]
  good = results_vec[6]
  strong = results_vec[7]
  happy = results_vec[8]
  trustworthy = results_vec[9]
  great = results_vec[10]
  
  meet = results_vec[11]
  sit_down = results_vec[12]
  interesting = results_vec[14]
  opportunist= results_vec[15]
  
  if(candidate == 'Trump')
  {
    cArraro_df = data.frame(TALikeable = likeable, TAGood = good, TAHappy = happy, TATrustworthy = trustworthy, TAActive = active,TAIntelligent = intelligent, TAPotent = potent,TAFast = fast,TAStrong = strong,TAGreat =great,TAMeet = meet, TASit = sit_down, TAInteresting = interesting, TAOpportunist = opportunist)
  }
  if (candidate == 'Clinton')
  {
    cArraro_df = data.frame(CALikeable = likeable, CAGood = good, CAHappy = happy, CATrustworthy = trustworthy, CAActive = active,CAIntelligent = intelligent, CAPotent = potent,CAFast = fast,CAStrong = strong,CAGreat =great,CAMeet = meet, CASit = sit_down, CAInteresting = interesting, CAOpportunist = opportunist)
  }
  return(cArraro_df)
}

authoritarian_index<-function(output_list,sub_nr)
{
  auth_vec<-as.numeric(output_list[[sub_nr]]$auth)
  
  #index is reversed
  for (i in 1:4)
  {if (auth_vec[i]==2)
  {auth_vec[i]=0}
  }
  
  auth_index = mean(auth_vec)
  auth_frame = data.frame(Auth = auth_index)
  return(auth_frame)
}


sophistication_index<-function(output_list,sub_nr)
{
  soph_vector = as.numeric(output_list[[sub_nr]]$soph)
  soph_vector = soph_vector - 1 
  soph_index = mean(soph_vector)
  soph_frame = data.frame(Soph = soph_index)
}

alignment_compiler<-function(output_list,sub_nr)
{
  align_vec<-as.numeric(output_list[[sub_nr]]$align)
  
  #AlignT
  AlignT = align_vec[1]
  #AlignC - REVERSED
  AlignC = align_vec[2]
  #AlignR
  AlignR = align_vec[3]
  #AlignD
  AlignD = align_vec[4]
  #LeftRight
  LeftRight = align_vec[5]
  #LibCon
  LibCon = align_vec[6]
  #DemRep
  DemRep = align_vec[7]
  
  align_df = data.frame(AlignT =AlignT,AlignC =AlignC,AlignR =AlignR ,AlignD=AlignD,LeftRight =LeftRight, LibCon =LibCon ,DemRep =DemRep)
}

action_compiler<-function(output_list,sub_nr)
{
  action_vec<-as.numeric(output_list[[sub_nr]]$action)
  
  #Registered
  if(action_vec[1]==2){registered = 'Yes'}else{registered = "No"}
  #Voted
  if(action_vec[2]==2){voted = 'Yes'}else{voted = "No"}
  #WhenDecided
  if(action_vec[4]==1){WhenDecided = "2Weeks"}
  else if(action_vec[4]==2){WhenDecided = "4Weeks"}
  else if(action_vec[4]==3){WhenDecided = "3Month"}
  else if(action_vec[4]==4){WhenDecided = "3+Month"}
  else if(action_vec[4]==5){WhenDecided = "No"}
  #DemPrime
  if(action_vec[5]==2){demprime= 'Yes'}else{demprime = "No"}
  #DemCan
  if(action_vec[6]==1){DemCan= 'Hillary'}
  else if(action_vec[6]==2){DemCan= 'Bernie'}
  else if(action_vec[6]==3){DemCan= 'Omalley'}
  else if(action_vec[6]==4){DemCan= 'No'}
  #RepPrime
  if(action_vec[7]==2){repprime= 'Yes'}else{repprime = "No"}
  #RepCan
  if(action_vec[8]==1){RepCan= 'Trump'}
  else if(action_vec[8]==2){RepCan= 'Cruz'}
  else if(action_vec[8]==3){RepCan= 'Rubio'}
  else if(action_vec[8]==4){RepCan= 'Kasich'}
  else if(action_vec[8]==5){RepCan= 'No'}
  #El2012
  if(action_vec[9]==1){El2012= 'Obama'}
  else if(action_vec[9]==2){El2012= 'Romney'}
  else if(action_vec[9]==3){El2012= 'Other'}
  else if(action_vec[9]==4){El2012= 'Too young'}
  else if(action_vec[9]==5){El2012= 'No vote'}
  #El2008
  if(action_vec[10]==1){El2008= 'Obama'}
  else if(action_vec[10]==2){El2008= 'McCain'}
  else if(action_vec[10]==3){El2008= 'Other'}
  else if(action_vec[10]==4){El2008= 'Too young'}
  else if(action_vec[10]==5){El2008= 'No vote'}
  #El2004
  if(action_vec[11]==1){El2004= 'Bush'}
  else if(action_vec[11]==2){El2004= 'Kerry'}
  else if(action_vec[11]==3){El2004= 'Other'}
  else if(action_vec[11]==4){El2004= 'Too young'}
  else if(action_vec[11]==5){El2004= 'No vote'}
  #El2000
  if(action_vec[12]==1){El2000= 'Bush'}
  else if(action_vec[12]==2){El2000= 'Gore'}
  else if(action_vec[12]==3){El2000= 'Other'}
  else if(action_vec[12]==4){El2000= 'Too young'}
  else if(action_vec[12]==5){El2000= 'No vote'}
action_df =data.frame(Registered = registered, Voted = voted,WhenDecided =WhenDecided,DemPrime=demprime,DemCan=DemCan,RepPrime=repprime,RepCan=RepCan,El2012=El2012,El2008=El2008,El2004=El2004,El2000=El2000, stringsAsFactors = FALSE)
return(action_df)
}

condition_compiler<-function(output_list,sub_nr)
{
cond_vec = output_list[[sub_nr]]$cond
video_nr =cond_vec[1]
reflect_nr = cond_vec[2]

if (video_nr == '1'){video = "TPositive"} 
else if (video_nr == '2'){video = "CPositive"} 
else if (video_nr == '3'){video = "TNegative"} 
else if (video_nr == '4'){video = "CNegative"} 
else if (video_nr == '0'){video = "CNegative"} 

if (reflect_nr == '1'){reflect = "OwnView"} 
else if (reflect_nr == '2'){reflect = "TAssigned"} 
else if (reflect_nr == '3'){reflect = "CAssigned"} 
else if (reflect_nr == '4'){reflect = "Control"} 
else if (video_nr == '0'){reflect = "Control"}

cond_df=data.frame(VideoCond = video, RefCond = reflect)
return(cond_df)
}

issue_checker <- function(output_list,sub_nr)
{
  control_vec<-output_list[[sub_nr]]$control
  checks=logical(length=2)
  checks[1] = if(control_vec[2]=="2"){T}else{F}
  checks[2] = if(control_vec[4]=="1"){T}else{F}
  if(sum(checks)==2)
  {check = T}else{check = F}
  
  #Who shot the video
  video_nr = output_list[[sub_nr]]$cond[1]
  if(video_nr == '1' |video_nr == '4' |video_nr == '0')
  {source="Trump"}else{source="Clinton"}
  #Who did they think shot the video
  subsource <- output_list[[sub_nr]]$check
  
  #Recognize Lofven
  lofven = control_vec[3]
  
  check_df = data.frame(Check=check,Source=source,SubSource=subsource,Lofven=lofven,stringsAsFactors = FALSE)
  return(check_df)
}

essay_compiler<-function(output_list,sub_nr)
{
  essay<-output_list[[sub_nr]]$essay
  essay_df = data.frame(Essay = essay, stringsAsFactors = FALSE)
  return(essay_df)
}