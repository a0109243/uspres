#CSVs for Export


#For Cronbach ----
attach(final)
Twarmth =  data.frame(TFWarm , TFTolerant , TFGoodNatured, TFSincere , 
             TALikeable,TAGood,TAHappy,TATrustworthy)

Cwarmth = data.frame(CFWarm , CFTolerant , CFGoodNatured, CFSincere , 
             CALikeable,CAGood,CAHappy,CATrustworthy)

Tcompetence = data.frame(TFIndependent,TFIntelligent,TFCompetent,TFCompetitive ,
                 TAActive,TAIntelligent,TAPotent,TAFast,TAStrong)
Ccompetence = data.frame(CFIndependent,CFIntelligent,CFCompetent,CFCompetitive ,
                 CAActive,CAIntelligent,CAPotent,CAFast,CAStrong)

TrumpUnclear = data.frame(TASit,TAMeet,TAInteresting, TAOpportunist,TAGreat)
ClintonUnclear = data.frame(CASit,CAMeet,CAInteresting, CAOpportunist,CAGreat)

detach(final)

write.csv(Twarmth, file = "Twarmth.csv",row.names=FALSE)
write.csv(Cwarmth, file = "Cwarmth.csv",row.names=FALSE)
write.csv(Tcompetence, file = "Tcompetence.csv",row.names=FALSE)
write.csv(Ccompetence, file = "CCompetence.csv",row.names=FALSE)
write.csv(TrumpUnclear, file = "TrumpUnclear.csv",row.names=FALSE)
write.csv(ClintonUnclear, file = "ClintonUnclear.csv",row.names=FALSE)

attach(main.df)

Twarmth =  data.frame(TFWarm , TFTolerant , TFGoodNatured, TFSincere , 
                      TALikeable,TAGood,TAHappy,TATrustworthy)

Cwarmth = data.frame(CFWarm , CFTolerant , CFGoodNatured, CFSincere , 
                     CALikeable,CAGood,CAHappy,CATrustworthy)

Tcompetence = data.frame(TFIndependent,TFIntelligent,TFCompetent,TFCompetitive ,
                         TAActive,TAIntelligent,TAPotent,TAFast,TAStrong)
Ccompetence = data.frame(CFIndependent,CFIntelligent,CFCompetent,CFCompetitive ,
                         CAActive,CAIntelligent,CAPotent,CAFast,CAStrong)

TrumpUnclear = data.frame(TASit,TAMeet,TAInteresting, TAOpportunist,TAGreat)
ClintonUnclear = data.frame(CASit,CAMeet,CAInteresting, CAOpportunist,CAGreat)



write.csv(Twarmth, file = "Twarmth2.csv",row.names=FALSE)
write.csv(Cwarmth, file = "Cwarmth2.csv",row.names=FALSE)
write.csv(Tcompetence, file = "Tcompetence2.csv",row.names=FALSE)
write.csv(Ccompetence, file = "CCompetence2.csv",row.names=FALSE)
write.csv(TrumpUnclear, file = "TrumpUnclear2.csv",row.names=FALSE)
write.csv(ClintonUnclear, file = "ClintonUnclear2.csv",row.names=FALSE)

detach(main.df)

attach(reversemain.df)

Twarmth =  data.frame(TFWarm , TFTolerant , TFGoodNatured, TFSincere , 
                      TALikeable,TAGood,TAHappy,TATrustworthy)

Cwarmth = data.frame(CFWarm , CFTolerant , CFGoodNatured, CFSincere , 
                     CALikeable,CAGood,CAHappy,CATrustworthy)

Tcompetence = data.frame(TFIndependent,TFIntelligent,TFCompetent,TFCompetitive ,
                         TAActive,TAIntelligent,TAPotent,TAFast,TAStrong)
Ccompetence = data.frame(CFIndependent,CFIntelligent,CFCompetent,CFCompetitive ,
                         CAActive,CAIntelligent,CAPotent,CAFast,CAStrong)

TrumpUnclear = data.frame(TASit,TAMeet,TAInteresting, TAOpportunist,TAGreat)
ClintonUnclear = data.frame(CASit,CAMeet,CAInteresting, CAOpportunist,CAGreat)



write.csv(Twarmth, file = "Twarmth3.csv",row.names=FALSE)
write.csv(Cwarmth, file = "Cwarmth3.csv",row.names=FALSE)
write.csv(Tcompetence, file = "Tcompetence3.csv",row.names=FALSE)
write.csv(Ccompetence, file = "CCompetence3.csv",row.names=FALSE)
write.csv(TrumpUnclear, file = "TrumpUnclear3.csv",row.names=FALSE)
write.csv(ClintonUnclear, file = "ClintonUnclear3.csv",row.names=FALSE)

detach(reversemain.df)


#For Regressions
attach(main.df)

regression2.df <- data.frame(Gender,Age,Income,Race,Hispanic,Schooling,Citizen, Married,Partisan, CCorrected,TCorrected,Support)
detach(main.df)
write.csv(regression2.df, file = "regression2.csv",row.names=FALSE)

attach(main.df)
attitude.regression.df <- data.frame(Twarmth,Tcompetence,Cwarmth,Ccompetence,CCorrected,C2Corrected)
detach(main.df)

write.csv(attitude.regression.df, file = "attitude.regression.csv",row.names=FALSE)
