// JavaScript Document


//whether userID is typed manually or assigned
var onlineVersion = 1;

// Create global variables
	//to log total time	
		var timeStarted = new Date();
	//Array with times for each subset in the experiment
	// 0: consent; 1: AMPinstructions; 2: AMP; 3: Video instruction; 4:Video; 5: Reflection; 6: AMPinstruction; 7: AMP2 8-13: Survey segments; 14: Full time
		var timeArray = []; 
	//contains preloaded stimuli
		var stimContainer = [];
		var charContainer = [];
		var otherContainer = [];
	// hardcoded links to fillers
		var mask = "http://www.chrisasplund.com/PCIP/MaskStim2.jpg";
		var empty = "http://www.chrisasplund.com/PCIP/Empty.jpg";
		var fixation = "http://www.chrisasplund.com/PCIP/Fixation.jpg";
		var Control = "http://www.chrisasplund.com/PCIP/Control.jpg";
	// a nrTrial x 2 array of answers, with pressed key in the first bin ( 1 = j, 2 = k) and total trial time in the second
		var	answerArray = [];
	// an array assigning conditions for the stimulus type: 0 is neutral, 1-4 is clinton, 5-8 is trump, 9-12 is Lofven
		var stimOrder = [];
		var stimOrder2 = []; 
	// an array assigning the order of Chinese characters.
		var chineseOrder = [];
		var chineseOrder2 = [];
	//	Create subjectnumber
	var userID = "";
	if (onlineVersion == 0){
		kkkkjk
		while(userID == "" || userID == null)
			{
			userID = prompt("Enter User ID:");
			}
	}
	else{
	  	var IDNum = randomIntFromInterval(10000,19999)
		var IDString = IDNum.toString();
	  	var videoNum = randomIntFromInterval (1,2);
	  	if (videoNum == 1){
		  	var videoID = "A";
	  	}
	  	else if (videoNum == 2){ 
		  	var videoID = "C";	
	  	}
		var promptNum = randomIntFromInterval(1,4);
		if (promptNum == 1){
		  	var promptID = "J";
	  	}
	  	else if (promptNum == 2){ 
			var promptID = "K";	
		}
		else if(promptNum == 3){
		  	var promptID = "L";
	  	}
	  	else if (promptNum == 4){ 
		  	var promptID = "M"	;
	  	}
		userID = IDString + videoID + promptID;
	  }
			
		
	function saveData() {
		timeArray[13] = new Date() - timeArray[12];
		timeArray[14] = new Date() - timeStarted;
		var d = [];
		var cT = [];
		var cC = [];
		fT = [];
		fC = [];
		alignment = [];
		action = [];
		sophistication = [];
		demo = [];
		au = [];
		control = [];
	cT[0] = document.querySelector('input[name = "cT1"]:checked').value;
	cT[1] = document.querySelector('input[name = "cT2"]:checked').value;
	cT[2] = document.querySelector('input[name = "cT3"]:checked').value;
	cT[3] = document.querySelector('input[name = "cT4"]:checked').value;
	cT[4] = document.querySelector('input[name = "cT5"]:checked').value;
	cT[5] = document.querySelector('input[name = "cT6"]:checked').value;
	cT[6] = document.querySelector('input[name = "cT7"]:checked').value;
	cT[7] = document.querySelector('input[name = "cT8"]:checked').value;
	cT[8] = document.querySelector('input[name = "cT9"]:checked').value;
	cT[9] = document.querySelector('input[name = "cT10"]:checked').value;
	cT[10] = document.querySelector('input[name = "cT11"]:checked').value;
	cT[11] = document.querySelector('input[name = "cT12"]:checked').value;
	cT[12] = document.querySelector('input[name = "cT13"]:checked').value;
	cT[13] = document.querySelector('input[name = "cT14"]:checked').value;
	cT[14] = document.querySelector('input[name = "cT15"]:checked').value;
	cC[0] = document.querySelector('input[name = "cC1"]:checked').value;
	cC[1] = document.querySelector('input[name = "cC2"]:checked').value;
	cC[2] = document.querySelector('input[name = "cC3"]:checked').value;
	cC[3] = document.querySelector('input[name = "cC4"]:checked').value;
	cC[4] = document.querySelector('input[name = "cC5"]:checked').value;
	cC[5] = document.querySelector('input[name = "cC6"]:checked').value;
	cC[6] = document.querySelector('input[name = "cC7"]:checked').value;
	cC[7] = document.querySelector('input[name = "cC8"]:checked').value;
	cC[8] = document.querySelector('input[name = "cC9"]:checked').value;
	cC[9] = document.querySelector('input[name = "cC10"]:checked').value;
	cC[10] = document.querySelector('input[name = "cC11"]:checked').value;
	cC[11] = document.querySelector('input[name = "cC12"]:checked').value;
	cC[12] = document.querySelector('input[name = "cC13"]:checked').value;
	cC[13] = document.querySelector('input[name = "cC14"]:checked').value;
	cC[14] = document.querySelector('input[name = "cC15"]:checked').value;
	fT[0] = document.querySelector('input[name = "fT1"]:checked').value;
	fT[1] = document.querySelector('input[name = "fT2"]:checked').value;
	fT[2] = document.querySelector('input[name = "fT3"]:checked').value;
	fT[3] = document.querySelector('input[name = "fT4"]:checked').value;
	fT[4] = document.querySelector('input[name = "fT5"]:checked').value;
	fT[5] = document.querySelector('input[name = "fT6"]:checked').value;
	fT[6] = document.querySelector('input[name = "fT7"]:checked').value;
	fT[7] = document.querySelector('input[name = "fT8"]:checked').value;
	fT[8] = document.querySelector('input[name = "fT9"]:checked').value;
	fC[0] = document.querySelector('input[name = "fC1"]:checked').value;
	fC[1] = document.querySelector('input[name = "fC2"]:checked').value;
	fC[2] = document.querySelector('input[name = "fC3"]:checked').value;
	fC[3] = document.querySelector('input[name = "fC4"]:checked').value;
	fC[4] = document.querySelector('input[name = "fC5"]:checked').value;
	fC[5] = document.querySelector('input[name = "fC6"]:checked').value;
	fC[6] = document.querySelector('input[name = "fC7"]:checked').value;
	fC[7] = document.querySelector('input[name = "fC8"]:checked').value;
	fC[8] = document.querySelector('input[name = "fC9"]:checked').value;
	alignment[0] = document.querySelector('input[name = "a1"]:checked').value;
	alignment[1] = document.querySelector('input[name = "a2"]:checked').value;
	alignment[2] = document.querySelector('input[name = "a3"]:checked').value;
	alignment[3] = document.querySelector('input[name = "a4"]:checked').value;
	alignment[4] = document.querySelector('input[name = "a5"]:checked').value;
	alignment[5] = document.querySelector('input[name = "a6"]:checked').value;
	alignment[6] = document.querySelector('input[name = "a7"]:checked').value;
	action[0] = document.querySelector('input[name = "p1"]:checked').value;
	action[1] = document.querySelector('input[name = "p2"]:checked').value;
	action[2] = document.querySelector('input[name = "p3"]:checked').value;
	action[3] = document.querySelector('input[name = "p4"]:checked').value;
	action[4] = document.querySelector('input[name = "p5"]:checked').value;
	action[5] = document.querySelector('input[name = "p6"]:checked').value;
	action[6] = document.querySelector('input[name = "p7"]:checked').value;
	action[7] = document.querySelector('input[name = "p8"]:checked').value;
	action[8] = document.querySelector('input[name = "p9"]:checked').value;
	action[9] = document.querySelector('input[name = "p10"]:checked').value;
	action[10] = document.querySelector('input[name = "p11"]:checked').value;
	action[11] = document.querySelector('input[name = "p12"]:checked').value;
	sophistication[0] = document.querySelector('input[name = "s1"]:checked').value;
	sophistication[1] = document.querySelector('input[name = "s2"]:checked').value;
	sophistication[2] = document.querySelector('input[name = "s3"]:checked').value;
	sophistication[3] = document.querySelector('input[name = "s4"]:checked').value;
	sophistication[4] = document.querySelector('input[name = "s5"]:checked').value;
	sophistication[5] = document.querySelector('input[name = "s6"]:checked').value;
	sophistication[6] = document.querySelector('input[name = "s7"]:checked').value;
	sophistication[7] = document.querySelector('input[name = "s8"]:checked').value;
	sophistication[8] = document.querySelector('input[name = "s9"]:checked').value;
	sophistication[9] = document.querySelector('input[name = "s10"]:checked').value;
	sophistication[10] = document.querySelector('input[name = "s11"]:checked').value;
	sophistication[11] = document.querySelector('input[name = "s12"]:checked').value;
	sophistication[12] = document.querySelector('input[name = "s13"]:checked').value;
	sophistication[13] = document.querySelector('input[name = "s14"]:checked').value;
	sophistication[14] = document.querySelector('input[name = "s15"]:checked').value;
	sophistication[15] = document.querySelector('input[name = "s16"]:checked').value;
	demo[0] = document.getElementById('d1').value;
	demo[1] = document.getElementById('d2').value;
	demo[2] = document.getElementById('d3').value;
	demo[3] = document.getElementById('d4').value;
	demo[4] = document.querySelector('input[name = "d5"]:checked').value;
	demo[5] = document.querySelector('input[name = "d6"]:checked').value;
	demo[6] = document.querySelector('input[name = "d7"]:checked').value;
	demo[7] = document.querySelector('input[name = "d8"]:checked').value;
	demo[8] = document.querySelector('input[name = "d9"]:checked').value;
	demo[9] = document.querySelector('input[name = "d10"]:checked').value;
	au[0] = document.querySelector('input[name = "au1"]:checked').value;
	au[1] = document.querySelector('input[name = "au2"]:checked').value;
	au[2] = document.querySelector('input[name = "au3"]:checked').value;
	au[3] = document.querySelector('input[name = "au4"]:checked').value;
	control[0] = document.querySelector('input[name = "c0"]:checked').value
	control[1] = document.querySelector('input[name = "c1"]:checked').value;
	control[2] = document.getElementById('c2').value;
	control[3] = document.querySelector('input[name = "c3"]:checked').value;
	control[4] = document.getElementById('c4').value;
	promptreply = document.getElementById('promptreply').value;
	attentioncheck = document.getElementById('attentioncheck').value;
		d = {
			"participant": userID,
			"conditions": [videoCondition,reflectionCondition],
			"promptanswer": promptreply,
			"attention":attentioncheck,
			"answers": answerArray,
			"stimuli": [stimOrder, stimOrder2],
			"characters": [chineseOrder,chineseOrder2],
			"carraro": [cT, cC],
			"fiske":   [fT, fC],
			"alignment": alignment,
			"action":  action,
			"sophistication": sophistication,
			"demographics": demo,
			"authoritarianism": au,
			"controls": control,
			"timeArray": timeArray,
			"comments": comments
		};
			sendToServer(userID, d);
		}



	
//Check Subject condition:
	// Videos 
	// A: Trump Positive; B: Hillary Positive; C: Trump Negative; D: Hillary Negative; arbitrarily defaults to Hillary Negative 
		userID = userID.toUpperCase();
			if (userID.indexOf('A') >= 0){
				videoCondition = 1; 
				videoStim = "http://www.chrisasplund.com/PCIP/Video/TrumpPositiveLow.mp4";
			} else if (userID.indexOf('B') >= 0){
				videoCondition = 2; 
				videoStim = "http://www.chrisasplund.com/PCIP/Video/ClintonPositiveLow.mp4";
			} else if (userID.indexOf('C') >= 0){
				videoCondition = 3; 
				videoStim = "http://www.chrisasplund.com/PCIP/Video/TrumpNegativeLow.mp4";
			} else if (userID.indexOf('D') >= 0){
				videoCondition = 4; 
				videoStim = "http://www.chrisasplund.com/PCIP/Video/ClintonNegativeLow.mp4";
			} else {
				videoCondition = 0;
				videoStim = "http://www.chrisasplund.com/PCIP/Video/ClintonNegativeLow.mp4";	
			}
	// Reflection Prompts
	// J: Own view reflection; K: Assigned viewpoint Hillary; L: Assigned viewpoint Trump; M: Control; arbitrarily defaults to control
			if (userID.indexOf('J') >= 0){
				reflectionCondition = 1; 
				reflectionParagraph = "Based on your own political views, what arguments could be made that your preferred major-party candidate should be the next president of the United States? Provide as many arguments as you can think of. You should provide both arguments in favor of your preferred candidate and arguments against the opposing major-party candidate.";
			} else if (userID.indexOf('K') >= 0){
				reflectionCondition = 2; 
				reflectionParagraph = "Putting aside your own political views, what arguments could be made that Donald Trump should be the next President of the United States? Provide as many arguments as you can think of. You should provide both arguments in favor of Donald Trump and arguments against Hillary Clinton.";
			} else if (userID.indexOf('L') >= 0){
				reflectionCondition = 3;  
				reflectionParagraph = "Putting aside your own political views, what arguments could be made that Hillary Clinton should be the next President of the United States? Provide as many arguments as you can think of. You should provide both arguments in favor of Hillary Clinton and arguments against Donald Trump.";
			} else if (userID.indexOf('M') >= 0){
				reflectionCondition = 4; 
				reflectionParagraph = "Based on your own views, what arguments could be made that cats or dogs make better pets? Provide as many arguments as you can think of.  You should provide both arguments in favor of your preferred pet and arguments against the other pet.";
			} else {
				reflectionCondition = 0;
				reflectionParagraph = "Based on your own views, what arguments could be made that cats or dogs make better pets? Provide as many arguments as you can think of.  You should provide both arguments in favor of your preferred pet and arguments against the other pet.";
			}





//Load all stimulus into containers in cache
function preloadStim() {
	for (i = 0; i < preloadStim.arguments.length; i++) {
		stimContainer[i] = new Image();
		stimContainer[i].src = preloadStim.arguments[i];
			}
		}
function preloadChar() {
	for (i = 0; i < preloadChar.arguments.length; i++) {
		charContainer[i] = new Image();
		charContainer[i].src = preloadChar.arguments[i];
			}
		}
function preloadOther() {
	for (i = 0; i < preloadOther.arguments.length; i++) {
		otherContainer[i] = new Image();
		otherContainer[i].src = preloadOther.arguments[i];
			}
		}

preloadOther(
 	"http://www.chrisasplund.com/PCIP/MaskStim.jpg",
 	"http://www.chrisasplund.com/PCIP/Empty.jpg",
	"http://www.chrisasplund.com/PCIP/Fixation.jpg",
	"http://www.chrisasplund.com/PCIP/Control.jpg",
	"http://www.chrisasplund.com/PCIP/MaskStim2.jpg"
	);


preloadStim(
	"http://www.chrisasplund.com/PCIP/Stim/Clinton1.JPG",
	"http://www.chrisasplund.com/PCIP/Stim/Clinton2.JPG",
	"http://www.chrisasplund.com/PCIP/Stim/Clinton3.JPG",
	"http://www.chrisasplund.com/PCIP/Stim/Clinton4.JPG",
	"http://www.chrisasplund.com/PCIP/Stim/Trump1.JPG",
	"http://www.chrisasplund.com/PCIP/Stim/Trump2.JPG",
	"http://www.chrisasplund.com/PCIP/Stim/Trump3.JPG",
	"http://www.chrisasplund.com/PCIP/Stim/Trump4.JPG",
	"http://www.chrisasplund.com/PCIP/Stim/Lofven1.JPG",
	"http://www.chrisasplund.com/PCIP/Stim/Lofven2.JPG",
	"http://www.chrisasplund.com/PCIP/Stim/Lofven3.JPG",
	"http://www.chrisasplund.com/PCIP/Stim/Lofven4.JPG"
			);
preloadChar(
	"http://www.chrisasplund.com/PCIP/Char/Slide0.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide1.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide2.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide3.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide4.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide5.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide6.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide7.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide8.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide9.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide10.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide11.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide12.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide13.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide14.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide15.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide16.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide17.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide18.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide19.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide20.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide21.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide22.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide23.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide24.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide25.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide26.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide27.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide28.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide29.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide30.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide31.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide32.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide33.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide34.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide35.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide36.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide37.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide38.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide39.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide40.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide41.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide42.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide43.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide44.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide45.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide46.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide47.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide48.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide49.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide50.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide51.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide52.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide53.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide54.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide55.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide56.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide57.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide58.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide59.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide60.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide61.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide62.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide63.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide64.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide65.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide66.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide67.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide68.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide69.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide70.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide71.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide72.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide73.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide74.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide75.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide76.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide77.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide78.JPG",
	"http://www.chrisasplund.com/PCIP/Char/Slide79.JPG"
	);

//Prepare trial order

	//Assign Stimuli and characters to trial numbers
	var stimArray = [];
	var stimOrder = []
	var chineseArray = [];
	  for (i = 0; i < 12; i++) { 
		  stimOrder[i] = i+1 ;
	  } 
	  stimOrder = stimOrder.concat([0,0,0,0]);
	  var stimDuplicate = stimOrder;
	  stimOrder = stimOrder.concat(stimDuplicate,stimDuplicate,stimDuplicate,stimDuplicate);
	  var chineseOrder = [];
	  for (i = 0; i < 80; i++) { 
		  chineseOrder[i] = i ;
	  } 
	  

	  for (i = 0; i < 12; i++) { 
		  stimOrder2[i] = i+1 ;
	  } 
	  stimOrder2 = stimOrder2.concat([0,0,0,0]);
	  var stimDuplicate = stimOrder2;
	  stimOrder2 = stimOrder2.concat(stimDuplicate,stimDuplicate,stimDuplicate,stimDuplicate);
	  var chineseOrder2 = [];
	  for (i = 0; i < 80; i++) { 
		  chineseOrder2[i] = i ;
	  } 	
	//Orders that will also be exported to check for condition
	shuffle(stimOrder);
	shuffle(stimOrder2);
	shuffle(chineseOrder2);
	shuffle(chineseOrder);
	//Create call list for stim and character, to be indexed against trial numbers
	 var stimLibArray = [
	"http://www.chrisasplund.com/PCIP/Control.jpg",
	"http://www.chrisasplund.com/PCIP/Stim/Clinton1.JPG",
	"http://www.chrisasplund.com/PCIP/Stim/Clinton2.JPG",
	"http://www.chrisasplund.com/PCIP/Stim/Clinton3.JPG",
	"http://www.chrisasplund.com/PCIP/Stim/Clinton4.JPG",
	"http://www.chrisasplund.com/PCIP/Stim/Trump1.JPG",
	"http://www.chrisasplund.com/PCIP/Stim/Trump2.JPG",
	"http://www.chrisasplund.com/PCIP/Stim/Trump3.JPG",
	"http://www.chrisasplund.com/PCIP/Stim/Trump4.JPG",
	"http://www.chrisasplund.com/PCIP/Stim/Lofven1.JPG",
	"http://www.chrisasplund.com/PCIP/Stim/Lofven2.JPG",
	"http://www.chrisasplund.com/PCIP/Stim/Lofven3.JPG",
	"http://www.chrisasplund.com/PCIP/Stim/Lofven4.JPG"
	  ];
	 
	
	var chineseArray = [];
	  for (i = 0; i < 80; i++) { 
		  chineseArray[i] = "http://www.chrisasplund.com/PCIP/Char/" + "Slide" + chineseOrder[i] + ".JPG";
	  }
	 for (i = 0; i < 80; i++) { 
		  stimArray[i] = stimLibArray[stimOrder[i]] ;
	  }
//A single trial
	function singleTrial(trialNum) {
	var trialStart = new Date()
	//so that unanswered questions return negative time on trialTime for easy exclusion
	var answerTime = -3000;
	var keyPress = 0;
	function animateTrial(frameCounter,trialEnd){
	frameCounter = frameCounter + 1; 
	if (frameCounter == 80){var trialEnd = new Date()}
		requestID = requestAnimationFrame(function(){animateTrial(frameCounter,trialEnd)});
	if (frameCounter >81){
		if(new Date() - trialStart>3300){
			cancelAnimationFrame(requestID);
			var trialTime = answerTime - trialEnd;
			recorder(keyPress,trialTime,trialNum);
			}
		else if (keyPress == 0){
			$(document).keyup(function(key){
			 	if (key.which == 74) { //j
				keyPress = 1;
				answerTime = new Date();
					}	
				else if (key.which == 75) { //k
				keyPress = 2;
				answerTime = new Date();
					}
				});
		}
	}
	if (frameCounter<61){
		document.getElementById("myImg").src = fixation ;
		}
	else if (frameCounter<61+5){
		document.getElementById("myImg").src = stimArray[trialNum];
		}
	else if (frameCounter<67+7){
		document.getElementById("myImg").src = empty;
		}
	else if (frameCounter<74+6){
		document.getElementById("myImg").src = chineseArray[trialNum];
		}
	else if (frameCounter<80+6){document.getElementById("myImg").src = mask;
	}
	}
	animateTrial(1,0);
}

//Whole AMP battery - runs the single trial function multiple times
function wholeAMP() {
	counter = 0;
	setIntervalX(function(){
		singleTrial(counter);
		counter +=1;
	},
	3500,80);
}



//I HATE doing this but I can't quite figure out variable assignments. So here are the same two functions as above, for the second AMP, only changes are display location and stimulus array.

	var stimArray2 = [];
	var chineseArray2 = [];
	  for (i = 0; i < 80; i++) { 
		  chineseArray2[i] = "http://www.chrisasplund.com/PCIP/Char/" + "Slide" + chineseOrder2[i] + ".JPG";
	  }

	 for (i = 0; i < 80; i++) { 
		  stimArray2[i] = stimLibArray[stimOrder2[i]] ;
	  }


function singleTrial2(trialNum2) {
	var trialStart = new Date()
	//so that unanswered questions return negative time on trialTime for easy exclusion
	var answerTime = -3000;
	var keyPress = 0;
	function animateTrial(frameCounter,trialEnd){
	frameCounter = frameCounter + 1; 
	if (frameCounter == 80){var trialEnd = new Date()}
		requestID = requestAnimationFrame(function(){animateTrial(frameCounter,trialEnd)});
	if (frameCounter >81){
		if(new Date() - trialStart>3300){
			cancelAnimationFrame(requestID);
			var trialTime = answerTime - trialEnd;
			recorder(keyPress,trialTime,trialNum2);
			}
		else if (keyPress == 0){
			$(document).keyup(function(key){
			 	if (key.which == 74) { //j
				keyPress = 1;
				answerTime = new Date();
					}	
				else if (key.which == 75) { //k
				keyPress = 2;
				answerTime = new Date();
					}
				});
		}
	}
	if (frameCounter<61){
		document.getElementById("myImg2").src = fixation ;
		}
	else if (frameCounter<61+5){
		document.getElementById("myImg2").src = stimArray2[trialNum2];
		}
	else if (frameCounter<67+7){
		document.getElementById("myImg2").src = empty;
		}
	else if (frameCounter<74+6){
		document.getElementById("myImg2").src = chineseArray2[trialNum2];
		}
	else if (frameCounter<80+6){document.getElementById("myImg2").src = mask;
	}
	}
	animateTrial(1,0);
	
}
	//Function to push response to global answerArray
	function recorder(keyPress,trialTime,trialNum) {
		answerArray.push([keyPress,trialTime]);
		console.log(answerArray[trialNum][0]);
		console.log(answerArray[trialNum][1]);
		//console.log(answerArray[trialNum+80][0]);
		//console.log(answerArray[trialNum+80][1]);
	}

//Whole AMP battery - runs the single trial function multiple times
function wholeAMP2() {
	counter2 = 0;
	setIntervalX(function(){
		singleTrial2(counter2);
		counter2 +=1;
	},
	3500,80);
}

//setInterval but runs only a fixed number of times, required to run a set number of trials
function setIntervalX(callback, delay, repetitions) {
    var x = 0;
    var intervalID = window.setInterval(function () {

       callback();

       if (++x === repetitions) {
           window.clearInterval(intervalID);
       }
    }, delay);
}






//Measure screen refresh rate
//function screenRefreshMeasure(){
//	var timeAbsoluteFrames = []
//	var timeBetweenFrames = []
//	var result = []
//	function innerLoop(refreshCounter){
//		timeAbsoluteFrames[refreshCounter] = new Date();
//		requestID = requestAnimationFrame(function(){innerLoop(refreshCounter)});
//		if (refreshCounter > 0){
//			timeBetweenFrames[refreshCounter-1] = timeAbsoluteFrames[refreshCounter] - timeAbsoluteFrames[refreshCounter-1];
//		}
//		if (refreshCounter > 10){
//			cancelAnimationFrame(requestID);
//			console.log(timeBetweenFrames);
//			return timeBetweenFrames;}
//		refreshCounter = refreshCounter + 1; 
//}
//	var result = innerLoop(0)
//	console.log(result)
//	setTimeout(function(){
//		console.log(result);
//		return result},10000)
//}
//var timeBetweenFrames = [];
//timeBetweenFrames = screenRefreshMeasure();
//console.log(timeBetweenFrames)
//setTimeout(function(){
//var sum = 0;
//function calcSum(){
//	for(var i = 0; i < timeBetweenFrames.length; i++){
//   		sum += timeBetweenFrames[i]; //don't forget to add the base
//		console.log(i)
//	}
//}
//calcSum();
//var avg = sum/timeBetweenFrames.length;
//console.log(sum)
//console.log(avg)
//
//},10000)

//shuffling functions
function randomIntFromInterval(min,max)
{
    return Math.floor(Math.random()*(max-min+1)+min);
}


function shuffle(array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
    return array;
}
 

/////To draw per frame instead of by set interval

// http://paulirish.com/2011/requestanimationframe-for-smart-animating/
// http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating
 
// requestAnimationFrame polyfill by Erik Möller
// fixes from Paul Irish and Tino Zijdel
 
(function() {
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame'] || window[vendors[x]+'CancelRequestAnimationFrame'];
    }
    if (!window.requestAnimationFrame){
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime + timeToCall); },
              timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };
	}
 
    if (!window.cancelAnimationFrame){
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
	}
}());







//Functions that bring you from one part of the experiment to the next

function consentYes(){
	//Time how long they looked at the consentForm
	timeArray[0] = new Date() - timeStarted;
	//Bring up the next screen
	$("#yes").css("background-color", "#B5D8EB");
	setTimeout(function(){
		$("#pis").hide();
		$("#ampInstructions").show();
		$("#buttonContainer").show();
		$("#yes").css("background-color", "#046D8B");
	//Define the video that's being watched to initialize preloading
		document.getElementById("myVideo").src = videoStim;
	//Define the reflection prompt.
		document.getElementById("reflectionPrompt").innerHTML = reflectionParagraph;
	}, 500)
}

function consentNo(){
	$("#no").css("background-color", "#B5D8EB");
	alert("You need to consent to proceed with the experiment.");
	setTimeout(location.reload(),1000)
}


function showAMP(){
	timeArray[1] = new Date() - timeArray[0];
	$("#ampInstructions").hide();
	document.getElementById("JButtonContent").innerHTML = ""
	document.getElementById("KButtonContent").innerHTML = ""
	$("#ampTrials").show();
	wholeAMP();
	window.scrollTo(0,0);
	setTimeout(function(){showVideoInstructions()},290000);
}

function showVideoInstructions(){
	timeArray[2] = new Date() - timeArray[1];
	$("#buttonContainer").hide();
	$("#ampTrials").hide();
	$("#videoInstructions").show();
	window.scrollTo(0,0);
}
function showVideo(){
	timeArray[3] = new Date() - timeArray[2];
	$("#videoInstructions").hide();
	$("#videoTrial").show();
	window.scrollTo(0,0);
	vid = document.getElementById("myVideo");
	vid.play();
}

function showReflection(){
	timeArray[4] = new Date() - timeArray[3];
	$("#videoTrial").hide();
	$("#reflection").show();
	window.scrollTo(0,0);
}

function showAMPInstructionsSecond(){
	timeArray[5] = new Date() - timeArray[4];
	$("#buttonContainer").show();
	$("#reflection").hide();
	$("#ampInstructions2").show();
	window.scrollTo(0,0);

}
function showAMPSecond(){
	timeArray[6] = new Date() - timeArray[5];
	$("#ampInstructions2").hide();
	$("#ampTrials2").show();
	wholeAMP2();
	setTimeout(function(){showCarraro()},290000)
}

function showCarraro(){
	timeArray[7] = new Date() - timeArray[6];
	$("#buttonContainer").hide();
		$("#ampTrials2").hide();
		$("#carraro").show();
		window.scrollTo(0,0);
}

function showFiske(){
	cT = []
	cC =[]
	cT[0] = document.querySelector('input[name = "cT1"]:checked').value;
	cT[1] = document.querySelector('input[name = "cT2"]:checked').value;
	cT[2] = document.querySelector('input[name = "cT3"]:checked').value;
	cT[3] = document.querySelector('input[name = "cT4"]:checked').value;
	cT[4] = document.querySelector('input[name = "cT5"]:checked').value;
	cT[5] = document.querySelector('input[name = "cT6"]:checked').value;
	cT[6] = document.querySelector('input[name = "cT7"]:checked').value;
	cT[7] = document.querySelector('input[name = "cT8"]:checked').value;
	cT[8] = document.querySelector('input[name = "cT9"]:checked').value;
	cT[9] = document.querySelector('input[name = "cT10"]:checked').value;
	cT[10] = document.querySelector('input[name = "cT11"]:checked').value;
	cT[11] = document.querySelector('input[name = "cT12"]:checked').value;
	cT[12] = document.querySelector('input[name = "cT13"]:checked').value;
	cT[13] = document.querySelector('input[name = "cT14"]:checked').value;
	cT[14] = document.querySelector('input[name = "cT15"]:checked').value;
	cC[0] = document.querySelector('input[name = "cC1"]:checked').value;
	cC[1] = document.querySelector('input[name = "cC2"]:checked').value;
	cC[2] = document.querySelector('input[name = "cC3"]:checked').value;
	cC[3] = document.querySelector('input[name = "cC4"]:checked').value;
	cC[4] = document.querySelector('input[name = "cC5"]:checked').value;
	cC[5] = document.querySelector('input[name = "cC6"]:checked').value;
	cC[6] = document.querySelector('input[name = "cC7"]:checked').value;
	cC[7] = document.querySelector('input[name = "cC8"]:checked').value;
	cC[8] = document.querySelector('input[name = "cC9"]:checked').value;
	cC[9] = document.querySelector('input[name = "cC10"]:checked').value;
	cC[10] = document.querySelector('input[name = "cC11"]:checked').value;
	cC[11] = document.querySelector('input[name = "cC12"]:checked').value;
	cC[12] = document.querySelector('input[name = "cC13"]:checked').value;
	cC[13] = document.querySelector('input[name = "cC14"]:checked').value;
	cC[14] = document.querySelector('input[name = "cC15"]:checked').value;
	if (cC.every(testZero) && cT.every(testZero)){
		timeArray[8] = new Date() - timeArray[7];
		$("#carraro").hide();
		$("#fiske").show()
		window.scrollTo(0,0);
	}
	else {
		alert("Please answer all questions before continuing");
	}
}

function showSurvey1(){
	fT = []
	fC =[]
	fT[0] = document.querySelector('input[name = "fT1"]:checked').value;
	fT[1] = document.querySelector('input[name = "fT2"]:checked').value;
	fT[2] = document.querySelector('input[name = "fT3"]:checked').value;
	fT[3] = document.querySelector('input[name = "fT4"]:checked').value;
	fT[4] = document.querySelector('input[name = "fT5"]:checked').value;
	fT[5] = document.querySelector('input[name = "fT6"]:checked').value;
	fT[6] = document.querySelector('input[name = "fT7"]:checked').value;
	fT[7] = document.querySelector('input[name = "fT8"]:checked').value;
	fT[8] = document.querySelector('input[name = "fT9"]:checked').value;
	fC[0] = document.querySelector('input[name = "fC1"]:checked').value;
	fC[1] = document.querySelector('input[name = "fC2"]:checked').value;
	fC[2] = document.querySelector('input[name = "fC3"]:checked').value;
	fC[3] = document.querySelector('input[name = "fC4"]:checked').value;
	fC[4] = document.querySelector('input[name = "fC5"]:checked').value;
	fC[5] = document.querySelector('input[name = "fC6"]:checked').value;
	fC[6] = document.querySelector('input[name = "fC7"]:checked').value;
	fC[7] = document.querySelector('input[name = "fC8"]:checked').value;
	fC[8] = document.querySelector('input[name = "fC9"]:checked').value;
	if (fC.every(testZero) && fT.every(testZero)){
		timeArray[9] = new Date() - timeArray[8];
		$("#fiske").hide();
		$("#survey1").show() 
		window.scrollTo(0,0);
	}
	else {
		alert("Please answer all questions before continuing");
	}
}
function showSurvey2(){
	alignment = [];
	alignment[0] = document.querySelector('input[name = "a1"]:checked').value;
	alignment[1] = document.querySelector('input[name = "a2"]:checked').value;
	alignment[2] = document.querySelector('input[name = "a3"]:checked').value;
	alignment[3] = document.querySelector('input[name = "a4"]:checked').value;
	alignment[4] = document.querySelector('input[name = "a5"]:checked').value;
	alignment[5] = document.querySelector('input[name = "a6"]:checked').value;
	alignment[6] = document.querySelector('input[name = "a7"]:checked').value;
	if (alignment.every(testZero)){
		timeArray[10] = new Date() - timeArray[9];
		$("#survey1").hide();
		$("#survey2").show();
		window.scrollTo(0,0);
	}
	else {
		alert("Please answer all questions before continuing");
	}
}
function showSurvey3(){
	action = []
	action[0] = document.querySelector('input[name = "p1"]:checked').value;
	action[1] = document.querySelector('input[name = "p2"]:checked').value;
	action[2] = document.querySelector('input[name = "p3"]:checked').value;
	action[3] = document.querySelector('input[name = "p4"]:checked').value;
	action[4] = document.querySelector('input[name = "p5"]:checked').value;
	action[5] = document.querySelector('input[name = "p6"]:checked').value;
	action[6] = document.querySelector('input[name = "p7"]:checked').value;
	action[7] = document.querySelector('input[name = "p8"]:checked').value;
	action[8] = document.querySelector('input[name = "p9"]:checked').value;
	action[9] = document.querySelector('input[name = "p10"]:checked').value;
	action[10] = document.querySelector('input[name = "p11"]:checked').value;
	action[11] = document.querySelector('input[name = "p12"]:checked').value;
	if (action.every(testZero)){
		timeArray[11] = new Date() - timeArray[10];
		$("#survey2").hide();
		$("#survey3").show();
		window.scrollTo(0,0);
	}
	else {
		alert("Please answer all questions before continuing");
	}

}
function showSurvey4(){
	sophistication = []
	sophistication[0] = document.querySelector('input[name = "s1"]:checked').value;
	sophistication[1] = document.querySelector('input[name = "s2"]:checked').value;
	sophistication[2] = document.querySelector('input[name = "s3"]:checked').value;
	sophistication[3] = document.querySelector('input[name = "s4"]:checked').value;
	sophistication[4] = document.querySelector('input[name = "s5"]:checked').value;
	sophistication[5] = document.querySelector('input[name = "s6"]:checked').value;
	sophistication[6] = document.querySelector('input[name = "s7"]:checked').value;
	sophistication[7] = document.querySelector('input[name = "s8"]:checked').value;
	sophistication[8] = document.querySelector('input[name = "s9"]:checked').value;
	sophistication[9] = document.querySelector('input[name = "s10"]:checked').value;
	sophistication[10] = document.querySelector('input[name = "s11"]:checked').value;
	sophistication[11] = document.querySelector('input[name = "s12"]:checked').value;
	sophistication[12] = document.querySelector('input[name = "s13"]:checked').value;
	sophistication[13] = document.querySelector('input[name = "s14"]:checked').value;
	sophistication[14] = document.querySelector('input[name = "s15"]:checked').value;
	sophistication[15] = document.querySelector('input[name = "s16"]:checked').value;
	if (sophistication.every(testZero)){
		timeArray[12] = new Date() - timeArray[11];
		$("#survey3").hide();
		$("#survey4").show();
		window.scrollTo(0,0);
	}
	else {
		alert("Please answer all questions before continuing");
	}
}
function endExpt(){
	demo =[];
	au = [];
	control = [];
	demo[0] = document.querySelector('input[name = "d5"]:checked').value;
	demo[1] = document.querySelector('input[name = "d6"]:checked').value;
	demo[2] = document.querySelector('input[name = "d7"]:checked').value;
	demo[3] = document.querySelector('input[name = "d8"]:checked').value;
	demo[4] = document.querySelector('input[name = "d9"]:checked').value;
	demo[5] = document.querySelector('input[name = "d10"]:checked').value;
	au[0] = document.querySelector('input[name = "au1"]:checked').value;
	au[1] = document.querySelector('input[name = "au2"]:checked').value;
	au[2] = document.querySelector('input[name = "au3"]:checked').value;
	au[3] = document.querySelector('input[name = "au4"]:checked').value;
	control[0] = document.querySelector('input[name = "c0"]:checked').value;
	control[1] = document.querySelector('input[name = "c1"]:checked').value;
	control[2] = document.querySelector('input[name = "c3"]:checked').value;
	if (demo.every(testZero) && au.every(testZero) && control.every(testZero) && 
		document.getElementById('d1').value != "" &&  document.getElementById('d2').value != ""&& document.getElementById('d3').value != ""&& document.getElementById('d4').value != "" )
		{
		saveData();
		$("#survey4").hide();
		$("#end").show();
		$("#code").show();
} else {
	alert("You need to fill up all the boxes.")
	}
}


function sendToServer(id, curData) {
	var dataToServer = {
		'id': id,
		'experimentName': 'PCIP',
		'curData': JSON.stringify(curData)
		};
	$.post("save.php", dataToServer, function() {
		alert( "success" );
	}, JSON)
	.fail(function(){
		alert("Please make sure to copy this code:  " + code + userID);
	});
}



function testZero(element, index, array){
	return element >0;
}
