file_location = "/Users/chris/Desktop/Data_Adrian_Experiment/Data/11040AK.txt"

DataSample2 <- read.delim(file_location, header=FALSE)
textfile = as.character(DataSample2[1,1])
parsed =strsplit(textfile,paste("[,]", "[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz]{3,}","[:]",sep = ""))
participant_name = parsed[[1]][1]
conditions = parsed[[1]][2]
essay = parsed[[1]][3]
condition_check = parsed[[1]][4]
AMP_results = parsed[[1]][5]
stimuli_list = parsed[[1]][6]
characters = parsed[[1]][7]
carraro = parsed[[1]][8]
fiske = parsed[[1]][9]
alignment = parsed[[1]][10]
action = parsed[[1]][11]
sophistication = parsed[[1]][12]
demographics = parsed[[1]][13]
authoritarianism = parsed[[1]][14]
controls = parsed[[1]][15]
timeArray = parsed[[1]][16]
comments = parsed[[1]][17]

#Parses out the photos for each AMP trial. Stimuli1 denotes the stimuli for first AMP, stimuli2 for second AMP
a = strsplit(stimuli_list, "\\[")
b = strsplit(a[[1]][3],"\\]")[[1]][1]
c = strsplit(a[[1]][4],"\\]")[[1]][1]
stimuli1 = as.numeric(strsplit(b, ",")[[1]])
stimuli2 = as.numeric(strsplit(c, ",")[[1]])
stimuli_done <- c(stimuli1,stimuli2)



#parses AMP results. Generates AMPanswers and AMPtimings as vectors
#textcleanup
a= strsplit(AMP_results,"")[[1]]
b = a[1:length(a)-1]
c = b[2:length(b)]
d = paste(c, collapse="")
e = strsplit(d, "\\[")[[1]]
f = e[2:length(e)]

AMPanswers = vector()
AMPtimings = vector()

#go through the string per trial 
for (i in 1:length(f))
{
  if (i != length(f))
  {
    tempstring = strsplit(f[i], "],")[[1]]
  }
  else
  {
    #last case has slightly different characters
    tempstring = strsplit(f[i], "]")[[1]]
  }
  templist = strsplit(tempstring,",")[[1]]
  AMPanswers[i] = as.integer(templist)[1]
  AMPtimings[i] = as.integer(templist)[2]
}

#Fiskedata.
#Cleanup
a = strsplit(fiske,"],\\[")[[1]]
b = gsub("\\[\\[","",a)
c = gsub("\\]\\]","",b)
#DOUBLECHECK THESE 
###!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
TrumpFiske = as.numeric(strsplit(c[1], ",")[[1]])
ClintonFiske = as.numeric(strsplit(c[2], ",")[[1]])

#Carraro
a = strsplit(carraro,"],\\[")[[1]]
b = gsub("\\[\\[","",a)
c = gsub("\\]\\]","",b)
#DOUBLECHECK THESE 
###!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
TrumpCarraro = as.numeric(strsplit(c[1], ",")[[1]])
ClintonCarraro = as.numeric(strsplit(c[2], ",")[[1]])


#The following all follow the same pattern. First, A removes
#the initial square bracket, B removes second square bracket, 
#C splits the remainder into a vector, 
#and as.numeric converts it to numeric. Some don't need numeric

#Alignment
alignment
a = strsplit(alignment,"\\[")[[1]][2]
b = strsplit(a,"]")[[1]][1]
c = strsplit(b,",")[[1]]
alignment_done = as.numeric(c)


#Action
action
a = strsplit(action,"\\[")[[1]][2]
b = strsplit(a,"]")[[1]][1]
c = strsplit(b,",")[[1]]
action_done = as.numeric(c)


#demographics
sophistication
a = strsplit(sophistication,"\\[")[[1]][2]
b = strsplit(a,"]")[[1]][1]
c = strsplit(b,",")[[1]]
sophistication_done = as.numeric(c)

#demographics
demographics
a = strsplit(demographics,"\\[")[[1]][2]
b = strsplit(a,"]")[[1]][1]
c = strsplit(b,",")[[1]]
demographics_done = c

#authoritarianism
authoritarianism
a = strsplit(authoritarianism,"\\[")[[1]][2]
b = strsplit(a,"]")[[1]][1]
c = strsplit(b,",")[[1]]
authoritarianism_done = as.numeric(c)

#controls
controls
a = strsplit(controls,"\\[")[[1]][2]
b = strsplit(a,"]")[[1]][1]
c = strsplit(b,",")[[1]]
controls_done = c

#times
timeArray
a = strsplit(timeArray,"\\[")[[1]][2]
b = strsplit(a,"]")[[1]][1]
c = strsplit(b,",")[[1]]
timeArray_done = as.numeric(c)

#conditions
conditions
a = strsplit(conditions,"\\[")[[1]][2]
b = strsplit(a,"]")[[1]][1]
c = strsplit(b,",")[[1]]
conditions_done = as.numeric(c)

#comments
comments

#to clean up my headspace
rm(a,action,alignment,authoritarianism,AMP_results,b,c,
   carraro,characters,conditions,controls,d,demographics,
   e,f,i,fiske,file_location,parsed,sophistication,stimuli_list,stimuli1,stimuli2,templist,
   tempstring,textfile,timeArray)


#Translating stimuli_done into categories. In old system, 0 is neutral, 1-4 is clinton, 5-8 is trump, 9-12 is Lofven
#In new System, A is neutral, B is Clinton, C Trump, D Lofven
stimuli_done
stimuli_class = vector()
for (i in 1:length(stimuli_done))
{
  if (stimuli_done[i] == 0)
  {
    stimuli_class[i] = "A"
  }
  else if(stimuli_done[i] >0 && stimuli_done[i] <5)
  {
    stimuli_class[i] = "B"
  }
  else if (stimuli_done[i] >4 && stimuli_done[i] <9)
  {
    stimuli_class[i] = "C"
  }
  else if (stimuli_done[i] >8)
  {
    stimuli_class[i] = "D"
  }
}
stimuli_class = as.factor(stimuli_class)

stimuli_df=data.frame(AMPanswers,AMPtimings,stimuli_done,stimuli_class)
colnames(stimuli_df) <- c("answers","timings","stimuli","category")
complete_list=list(stimuli_df = stimuli_df, action = action_done, align = alignment_done, auth = authoritarianism_done,CCarra = ClintonCarraro,CFiske= ClintonFiske,TCarra = TrumpCarraro,TFiske = TrumpFiske, cond = conditions_done, soph =sophistication_done, times = timeArray_done,demo = demographics_done, control = controls_done, check = condition_check,essay = essay)

return(complete_list)
}